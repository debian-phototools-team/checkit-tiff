//
// Created by romeyke on 15.01.23.
//
#define T int
#define PRINT_TYPE(a,b) printf("Stackpos: %i, Value: %i\n", a,b);
#include "template_stack.h"
#include <assert.h>
int main() {
    int_stack_t * stack = int_new();
    /* empty */
    int_print_top(stack);
    assert(int_is_empty(stack));
    /* one value */
    int_push(stack, 1);
    assert(!int_is_empty(stack));
    int_print_top(stack);
    assert(1 ==  int_pop(stack));
    assert(int_is_empty(stack));
    /* two values */
    int_push(stack, 1);
    int_push(stack, 2);
    int_print_top(stack);
    assert(2 ==  int_pop(stack));
    assert(!int_is_empty(stack));
    int_print_top(stack);
    int_push_back(stack, 2);
    int_print_top(stack);
    /* -- */
    assert(2 == int_size(stack));
    int_increase_capacity(stack);
    assert(64== stack->capacity);
    assert(2 == int_size(stack));
    int_reset_capacity(stack);
    assert(32== stack->capacity);
    assert(2 == int_size(stack));
    //int_delete(stack);
    //assert(stack == NULL);

    return 0;
}
