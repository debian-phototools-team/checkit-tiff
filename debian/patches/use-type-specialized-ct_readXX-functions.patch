From b8a85f83d9e767d08d8c390048a343e5d2a0d209 Mon Sep 17 00:00:00 2001
From: Andreas Romeyke <art1pirat@fsfe.org>
Date: Thu, 26 Oct 2023 13:53:10 +0200
Subject: [PATCH] - use type specialized ct_readXX() functions - fixed macro
 using brackets around parameters

---
 src/lib/helper/check_tiffparse.c | 62 +++++++++++++++++++++++++-------
 1 file changed, 50 insertions(+), 12 deletions(-)

diff --git a/src/lib/helper/check_tiffparse.c b/src/lib/helper/check_tiffparse.c
index e4ff1d3..3a4b128 100644
--- a/src/lib/helper/check_tiffparse.c
+++ b/src/lib/helper/check_tiffparse.c
@@ -99,37 +99,74 @@ static ssize_t ct_read_intern(ctiff_t * ctif, void *buf, size_t count) {
 #endif
     return no_file_found; /* should not occur */
 }
 
 ssize_t ct_read8(ctiff_t * ctif, uint8 *buf, size_t byte_count) {
     return ct_read_intern(ctif, buf, byte_count);
 }
+ssize_t ct_read8s(ctiff_t * ctif, int8 *buf, size_t byte_count) {
+    return ct_read_intern(ctif, buf, byte_count);
+}
 
 ssize_t ct_read16(ctiff_t * ctif, uint16 *buf, size_t word_count) {
     size_t count = word_count * sizeof(uint16);
     ssize_t rdbytes = ct_read_intern(ctif, buf, count);
-            for (ssize_t i = 0; i < word_count; i++) {
-                uint16 * tmpbuf = buf+i;
-                *tmpbuf = htole16( *tmpbuf );
-            }
-            return rdbytes;
+    for (size_t i = 0; i < word_count; i++) {
+        uint16 *tmpbuf = buf + i;
+        *tmpbuf = htole16(*tmpbuf);
+    }
+    return rdbytes;
+}
+ssize_t ct_read16s(ctiff_t * ctif, int16 *buf, size_t word_count) {
+    size_t count = word_count * sizeof(int16);
+    ssize_t rdbytes = ct_read_intern(ctif, buf, count);
+    for (size_t i = 0; i < word_count; i++) {
+        int16 *tmpbuf = buf + i;
+        /* unsure if this works as expected on negative values */
+        *tmpbuf = (int16) htole16((uint16) *tmpbuf);
+    }
+    return rdbytes;
 }
 
 ssize_t ct_read32(ctiff_t * ctif, uint32 *buf, size_t quad_count) {
     size_t count = quad_count * sizeof(uint32);
     ssize_t rdbytes = ct_read_intern(ctif, buf, count);
     //printf("buf32: %lu (0x%x) quadcount=%lu count=%lu rdbytes=%lu\n", *buf, *buf, quad_count, count, rdbytes);
-    for (ssize_t i = 0; i < quad_count; i++) {
+    for (size_t i = 0; i < quad_count; i++) {
         uint32 * tmpbuf = buf+i;
         *tmpbuf = htole32( *tmpbuf );
     }
     //printf("buf32': %lu (0x%x)\n", *buf, *buf);
     return rdbytes;
 }
 
+ssize_t ct_read32s(ctiff_t * ctif, int32 *buf, size_t quad_count) {
+    size_t count = quad_count * sizeof(int32);
+    ssize_t rdbytes = ct_read_intern(ctif, buf, count);
+    //printf("buf32: %lu (0x%x) quadcount=%lu count=%lu rdbytes=%lu\n", *buf, *buf, quad_count, count, rdbytes);
+    for (size_t i = 0; i < quad_count; i++) {
+        int32 * tmpbuf = buf+i;
+        /* unsure if this works as expected on negative values */
+        *tmpbuf = (int32) htole32((uint32) *tmpbuf );
+    }
+    //printf("buf32': %lu (0x%x)\n", *buf, *buf);
+    return rdbytes;
+}
+
+ssize_t ct_readf(ctiff_t * ctif, float *buf, size_t float_count) {
+    size_t count = float_count * sizeof(float);
+    ssize_t rdbytes = ct_read_intern(ctif, buf, count);
+    return rdbytes;
+}
+
+ssize_t ct_readd(ctiff_t * ctif, double *buf, size_t double_count) {
+    size_t count = double_count * sizeof(double);
+    ssize_t rdbytes = ct_read_intern(ctif, buf, count);
+    return rdbytes;
+}
 
 int TIFFGetRawTagListIndex(ctiff_t * ctif, tag_t tag) { /* find n-th entry in IFD for given tag, return -1 if not found */
   switch (ctif->tagorder) {
     case has_unsorted_tags:
     case has_sorted_tags:
         break;
     case unknown_tag_order:
@@ -365,30 +402,31 @@ tag_t TIFFGetRawTagListEntry(ctiff_t * ctif, int tagidx ) {
     }
     if (is_byteswapped(ctif)) {
         TIFFSwabShort(&tag);
     }
     return tag;
 }
 
+/* offset data is pointer to memory of values of type offset_type */
 #define OFFSET_MALLOC(ctread, ctif_p, offsetdata, offset_type, count ) {\
-  size_t size = (size_t) sizeof(offset_type) * count;                   \
+  size_t size = (size_t) sizeof(offset_type) * (count);                   \
   /*printf("OFFSET_MALLOC: count=%lu, size=%lu\n", count, size); */                      \
   if((size_t) (ctif_p)->streamlen < size) {\
     char msg[VALUESTRLEN]; \
     snprintf(msg, VALUESTRLEN,  "TIFF Offset ct_read error, try to read from offset count=%zu bytes, but file has size=%lu\n", size, (ctif_p)->streamlen); \
     *ret_p  = set_value_found_ret( ret_p, str(msg)); \
     ret_p->returncode = tiff_seek_error_offset;\
     return * ret_p;\
   }\
   (offsetdata) = NULL; (offsetdata) = malloc (size);\
   if (NULL == (offsetdata)) {\
     fprintf( stderr, "could not allocate memory for offset_t\n");\
     exit (EXIT_FAILURE);\
   }\
-  ssize_t result = ctread( ctif_p, offsetdata, count);                   \
+  ssize_t result = ctread( ctif_p, (offsetdata), count);                   \
   /*printf("RESULT=%lu\n", result); */                                                                     \
   if (result == 0) {\
     char msg[VALUESTRLEN]; \
     snprintf(msg, VALUESTRLEN,  "TIFF Offset ct_read error, try to read from offset count=%zu bytes, but EOF detected\n", size); \
     *ret_p  = set_value_found_ret( ret_p, str(msg)); \
     ret_p->returncode = tiff_read_error_offset; \
     return *ret_p; \
@@ -457,41 +495,41 @@ ret_t read_offsetdata(ctiff_t * ctif, uint32 address, uint32 count, uint16 datat
       OFFSET_MALLOC(ct_read_intern, ctif, offset_p->datas8p, int8, count)
       break;
     case TIFF_SHORT: /* 16-bit unsigned integer */
       OFFSET_MALLOC(ct_read16, ctif, offset_p->data16p, uint16, count)
       offset_swabshort(ctif, offset_p->data16p, count);
       break;
     case TIFF_SSHORT: /* !16-bit signed integer */
-      OFFSET_MALLOC(ct_read16, ctif, offset_p->datas16p, int16, count)
+      OFFSET_MALLOC(ct_read16s, ctif, offset_p->datas16p, int16, count)
       offset_swabshort(ctif, (uint16 *) offset_p->datas16p, count);
       break;
     case TIFF_LONG: /* 32-bit unsigned integer */
     case TIFF_IFD: /* %32-bit unsigned integer (offset) */
       OFFSET_MALLOC(ct_read32, ctif, offset_p->data32p, uint32, count)
       offset_swablong(ctif, offset_p->data32p, count);
       break;
     case TIFF_SLONG: /* !32-bit signed integer */
-      OFFSET_MALLOC(ct_read32, ctif, offset_p->datas32p, uint32, count)
+      OFFSET_MALLOC(ct_read32s, ctif, offset_p->datas32p, int32, count)
       offset_swablong(ctif, (uint32 *) offset_p->data32p, count);
       break;
     case TIFF_RATIONAL: /* 64-bit unsigned fraction */
       //printf("TIFF RATIONAL, count=%lu\n", count);
       OFFSET_MALLOC(ct_read32, ctif, offset_p->data32p, uint32, 2*count) /* because numerator + denominator */
       offset_swablong(ctif, offset_p->data32p, 2*count);
       break;
     case TIFF_SRATIONAL: /* !64-bit signed fraction */
       fprintf(stderr, "offsetdata datatype=%i not supported yet", datatype);
       exit(EXIT_FAILURE);
     case TIFF_FLOAT: /* !32-bit IEEE floating point */
       assert( sizeof(float) == 4);
-      OFFSET_MALLOC(ct_read32, ctif, offset_p->datafloatp, float, count)
+      OFFSET_MALLOC(ct_readf, ctif, offset_p->datafloatp, float, count)
       break;
     case TIFF_DOUBLE: /* !64-bit IEEE floating point */
       assert( sizeof(double) == 8);
-      OFFSET_MALLOC(ct_read_intern, ctif, offset_p->datadoublep, double, count)
+      OFFSET_MALLOC(ct_readd, ctif, offset_p->datadoublep, double, count)
       break;
     case TIFF_LONG8: /* BigTIFF 64-bit unsigned integer */
     case TIFF_IFD8: /* BigTIFF 64-bit unsigned integer (offset) */
       assert( sizeof(double) == 8);
       OFFSET_MALLOC(ct_read_intern, ctif, offset_p->data64p, uint64, count)
       break;
     case TIFF_SLONG8: /* BigTIFF 64-bit signed integer */
-- 
2.43.0

