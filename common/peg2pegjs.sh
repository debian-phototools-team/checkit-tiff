#!/bin/bash
# 'checkit_tiff' is a conformance checker for baseline TIFFs
#
# author: Andreas Romeyke, 2015-2022
# licensed under conditions of libtiff
# (see http://libtiff.maptools.org/misc.html)
#
#
#
# this converts a PEG grammar in a PEG.js grammar
fgrep -v "set_parse_error" | sed -e "s/^#.*//g" -e "s/<-/=/g" -e "s/[<>]//g" -e "s/[\t ]#//g"
