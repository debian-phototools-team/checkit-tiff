/* 'checkit_tiff' is a conformance checker for baseline TIFFs
 *
 * author: Andreas Romeyke, 2015-2022
 * licensed under conditions of libtiff
 * (see http://libtiff.maptools.org/misc.html)
 *
 */

#ifndef CHECKIT_TIFF_PARSER_STATES
#define CHECKIT_TIFF_PARSER_STATES
#include "check.h"
#include "msg_parser.h"
#include "parser_types.h"

typedef struct full_res_s {
    int lineno;
    function_t function;
    returncode_t returncode;
    tag_t tag;
    string_t expected_value;
    string_t found_value;
} full_res_t;
#endif
