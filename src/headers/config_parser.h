/* 'checkit_tiff' is a conformance checker for baseline TIFFs
 *
 * author: Andreas Romeyke, 2015-2022
 * licensed under conditions of libtiff
 * (see http://libtiff.maptools.org/misc.html)
 *
 */

#ifndef FIXIT_TIFF_CONFIG_PARSER
#define FIXIT_TIFF_CONFIG_PARSER
#include "check.h"
#include "msg_parser.h"
#include "parser_types.h"
#include "parser_states.h"

#define MAXINCLUDEDEPTH 1

#define MAXSTACKDEPTH 65536u

typedef struct {
    internal_stacktype_t type;
    union{
        unsigned  int i;
/*        values_t val; */
        const char * regex;
    } value;
} internal_stack_entry_t;



#define T internal_stack_entry_t
void print_st (int i, internal_stack_entry_t  v);
#define PRINT_TYPE(a,b) print_st(a,b)
#include "template_stack.h"

typedef struct exe_entry_s {
  int lineno;
  bool is_precondition;
  tag_t tag;
  function_t function;
  internal_stack_entry_t_stack_t * stack;
} exe_entry_t;

#define T full_res_t
void print_res (int i, full_res_t v);
#define PRINT_TYPE(a,b) print_res(a,b)
#include "template_stack.h"

#define T values_t
void print_val (int i, values_t v);
#define PRINT_TYPE(a,b) print_val(a,b)
#include "template_stack.h"

#define T exe_entry_t
void print_exe (int i, exe_entry_t v);
#define PRINT_TYPE(a,b) print_exe(a,b)
#include "template_stack.h"

typedef struct parser_state_s {
  // TODO: Anzahl le-Werte für Tupel in Stack speichern
  int lineno;
  int valuelist;
  tag_t tag;
  tag_t tagref;
  reference_t any_reference;
  requirements_t req;
  modes_t mode;
  int logical_elements;
  short int includedepth;
  bool within_logical_or;
  FILE * stream;
  const char * cfg_filename;
  const char * regex_string;
  int called_tags[MAXTAGS];
  full_res_t_stack_t * result_stack;                   /* stores results from exe */
  values_t_stack_t * values_stack;                             /* stores expected value types */
  internal_stack_entry_t_stack_t * intermediate_stack; /* stores intermediate values */
  exe_entry_t_stack_t * exe_stack;                     /* stores exe function */
} parser_state_t;

static void helper_push_result(full_res_t res);
static full_res_t helper_pop_result();
static void helper_mark_top_n_results(int n, returncode_t type);
static full_res_t helper_get_nth(int n);
static ret_t call_exec_function(ctiff_t * ctif, ret_t * retp, const exe_entry_t * exep);
static void set_parse_error(char * msg, char * yytext);
void execute_plan (ctiff_t * ctif);
void print_plan ();
ret_t print_plan_results (retmsg_t *);
void clean_plan ();
void parse_plan ();
void parse_plan_via_stream (FILE * file, const char *optional_cfg_file);
void parse_plan_via_file (const char * cfg_file);
static void add_default_rules_to_plan();
static void set_include( const char *);
void exe_printstack_human_readable ();
static void result_push(full_res_t);
#endif
/* _FIXIT_TIFF_CONFIG_PARSER */
