/* 'checkit_tiff' is a conformance checker for baseline TIFFs
 *
 * author: Andreas Romeyke, 2015-2022
 * licensed under conditions of libtiff
 * (see http://libtiff.maptools.org/misc.html)
 *
 */

#include "check.h"

/*
#define DEBUG
*/


ret_t check_tag_has_value(ctiff_t * ctif, tag_t tag, unsigned int value) {
  return check_tag_has_value_quiet( ctif, tag, value);
}



/* vim: set tabstop=2 softtabstop=2 shiftwidth=2 smarttab expandtab :*/
