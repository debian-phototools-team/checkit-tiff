/* 'checkit_tiff' is a conformance checker for baseline TIFFs
 *
 * author: Andreas Romeyke, 2015-2022
 * licensed under conditions of libtiff
 * (see http://libtiff.maptools.org/misc.html)
 *
 */

#ifndef FIXIT_TIFF_MSG_PARSER
#define FIXIT_TIFF_MSG_PARSER
typedef enum {
    fc_true,
    fc_false,
    fc_tag_has_some_of_these_values,
    fc_tag_has_valuelist,
    fc_tag_has_value_in_range,
    fc_tag_has_value,
    fc_tag_has_value_quiet,
    fc_tag,
    fc_tag_quiet,
    fc_notag,
    fc_tag_has_valid_type,
    fc_datetime,
    fc_icc,
    fc_has_only_one_ifd,
    fc_tagorder,
    fc_tag_has_valid_asciivalue,
    fc_tag_has_value_matching_regex,
    fc_all_offsets_are_word_aligned,
    fc_all_offsets_are_used_once_only,
    fc_all_IFDs_are_word_aligned,
    fc_internal_logic_combine_open,
    fc_internal_logic_combine_close,
    fc_all_offsets_are_not_zero,
    fc_all_geotiff_tags_have_same_count_of_values,
    fc_check_cardinality,
    fc_dummy
} function_t;

const char * get_parser_function_description (function_t f);
const char * get_parser_error_description( returncode_t r);
const char * get_parser_function_name( function_t f );
#endif


/* vim: set tabstop=2 softtabstop=2 shiftwidth=2 smarttab expandtab :*/
