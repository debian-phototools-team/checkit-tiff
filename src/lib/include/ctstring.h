/* 'checkit_tiff' is a conformance checker for baseline TIFFs
 *
 * author: Andreas Romeyke, 2015-2022
 * licensed under conditions of libtiff
 * (see http://libtiff.maptools.org/misc.html)
 *
 */

#ifndef CHECKIT_TIFF_STRING
#define CHECKIT_TIFF_STRING
#include <string.h>
#include <stdbool.h>

typedef struct string_s {
    char * string;
    bool is_const:1;
    unsigned int len: 31;
} string_t;

#define INITIAL_STRINGBUFF_SIZE 4096
#define STRINGBUF_SIZE (65536*100)
typedef struct stringbuf_s {
    string_t * strings;
    size_t bufsize;
    size_t pos;
} stringbuf_t;

string_t str(const char *);
string_t const_str(const char *);
string_t empty_str();
size_t strlen_of_str (string_t);
const char * cstr_of_str (string_t);
stringbuf_t secstrcat_string (stringbuf_t dest, string_t src);
stringbuf_t secstrcat_cstr (stringbuf_t dest, const char * src);
stringbuf_t  empty_stringbuf();
void clean_str(string_t * s);
void clean_stringbuf(stringbuf_t * buf);
void clean_stringbuf_with_all_strings(stringbuf_t * buf);

#endif
