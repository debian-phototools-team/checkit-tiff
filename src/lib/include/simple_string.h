/* 'checkit_tiff' is a conformance checker for baseline TIFFs
 *
 * author: Andreas Romeyke, 2015-2022
 * licensed under conditions of libtiff
 * (see http://libtiff.maptools.org/misc.html)
 *
 * specific string implementation
 */

#ifndef CHECKIT_TIFF_PARSERSTRING
#define CHECKIT_TIFF_PARSERSTRING
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "tiff.h"

unsigned long atoul (const char * s);
unsigned int count_multiple_zero_bytes(const char *val, uint32 count);
char * sprint_string_as_hexdump(const char * val, uint32);
#endif
