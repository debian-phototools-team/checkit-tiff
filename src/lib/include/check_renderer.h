/* 'checkit_tiff' is a conformance checker for baseline TIFFs
 *
 * author: Andreas Romeyke, 2015-2022
 * licensed under conditions of libtiff
 * (see http://libtiff.maptools.org/misc.html)
 *
 */

#ifndef FIXIT_TIFF_CHECK_RENDERER
#define FIXIT_TIFF_CHECK_RENDERER

#include "check_helper.h"

/* 65536 Tags a 256 chars message + 100 Messages */
/* #define RENDERSIZE (65536*256 + 100*256) */
#define RENDERMINSIZE (512)
#define RENDERMAXSIZE (33554432)
typedef enum{ render_default, render_ansi, render_csv} render_type;
typedef enum{ within_valid, within_error, within_file, within_summaryerror, within_harderror } render_context_t;

stringbuf_t renderer ( const retmsg_t * ret );
void set_renderer_to_ansi();
void set_renderer_to_quiet();
void set_renderer_to_csv();
bool check_if_quiet(const retmsg_t *startp, short rc_is_valid);
stringbuf_t renderer_ansi ( const retmsg_t * ret);
stringbuf_t renderer_default ( const retmsg_t * ret);
stringbuf_t renderer_csv ( const retmsg_t * ret);
#endif

/* vim: set tabstop=2 softtabstop=2 shiftwidth=2 smarttab expandtab :*/
