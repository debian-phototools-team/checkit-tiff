/* 'checkit_tiff' is a conformance checker for baseline TIFFs
 *
 * author: Andreas Romeyke, 2015-2022
 * licensed under conditions of libtiff
 * (see http://libtiff.maptools.org/misc.html)
 *
 */

#ifndef FIXIT_TIFF_MSG_TIFFPARSE
#define FIXIT_TIFF_MSG_TIFFPARSE
#include "check.h"
#include "ctstring.h"
const char * TIFFTypeName( uint16 tagtype );
const char * TIFFTagName( tag_t tag );
#endif
/* vim: set tabstop=2 softtabstop=2 shiftwidth=2 smarttab expandtab :*/
