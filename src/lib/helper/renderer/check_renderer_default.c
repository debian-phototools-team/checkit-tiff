/* 'checkit_tiff' is a conformance checker for baseline TIFFs
 *
 * author: Andreas Romeyke, 2015-2022
 * licensed under conditions of libtiff
 * (see http://libtiff.maptools.org/misc.html)
 *
 */

#include "check_renderer.h"
#include "ctstring.h"
#include <assert.h>


stringbuf_t renderer_default ( const retmsg_t * ret ) {
  assert (ret != NULL);
  stringbuf_t res = empty_stringbuf();
  const retmsg_t * startp = ret;
  short int rc_is_valid = -1; // tristate: -1 undef, 0 valid, 1 invalid
  // print header
  res = secstrcat_cstr(res, "\n");
  res = secstrcat_cstr(res, "result IFD/tag \t--> rule description [; ");
  res = secstrcat_cstr(res, "expected specific values");
  res = secstrcat_cstr(res, "] [");
  res = secstrcat_cstr(res, "error description");
  res = secstrcat_cstr(res, "] ");
  res = secstrcat_cstr(res, "(lineno in config)");
  res = secstrcat_cstr(res, "\n");
  res = secstrcat_cstr( res, "----------------------------------------------------------------------------\n");
  while (NULL != startp) {
      if (startp->rm_type == rm_is_valid) {rc_is_valid = 0;}
      else if (startp->rm_type == rm_error || startp->rm_type == rm_hard_error) {rc_is_valid = 1;}
      if ( check_if_quiet(startp, rc_is_valid) ) { /*do nothing */ }
      else {
          switch (startp->rm_type) {
              case rm_rule:
                  res = secstrcat_cstr(res, "\t--> ");
                  res = secstrcat_string(res, startp->rm_msg);
                  break;
              case rm_tag:
              case rm_mode:
                  res = secstrcat_cstr(res, "\t");
                  res = secstrcat_string(res, startp->rm_msg);
                  break;
              case rm_value:
                  if (startp->rm_msg.len > 0) {
                      res = secstrcat_cstr(res, "; found: ");
                      res = secstrcat_string(res, startp->rm_msg);
                  }
                  break;
              case rm_expected:
                  if (startp->rm_msg.len > 0) {
                      res = secstrcat_cstr(res, "; expected: ");
                      res = secstrcat_string(res, startp->rm_msg);
                  }
                  break;
              case rm_hard_error:
                  res = secstrcat_cstr(res, "(HE)");
                  res = secstrcat_string(res, startp->rm_msg);
                  break;
              case rm_error:
                  res = secstrcat_cstr(res, "(EE)");
                  res = secstrcat_string(res, startp->rm_msg);
                  break;
              case rm_error_description:
                  res = secstrcat_cstr(res, " ");
                  res = secstrcat_string(res, startp->rm_msg);
                  break;
              case rm_warning:
                  res = secstrcat_cstr(res, "(WW)");
                  res = secstrcat_string(res, startp->rm_msg);
                  break;
              case rm_logicalor_error:
                  res = secstrcat_cstr(res, "(LE)");
                  res = secstrcat_string(res, startp->rm_msg);
                  break;
              case rm_file:
                  res = secstrcat_cstr(res, "file: ");
                  res = secstrcat_string(res, startp->rm_msg);
                  res = secstrcat_cstr(res, "\n");
                  break;
              case rm_lineno:
                  res = secstrcat_cstr(res, " (lineno: ");
                  res = secstrcat_string(res, startp->rm_msg);
                  res = secstrcat_cstr(res, ")");
                  break;
              case rm_endrule:
              case rm_endtiff:
                  res = secstrcat_cstr(res, "\n");
                  rc_is_valid = -1;
                  break;
              case rm_is_valid:
                  res = secstrcat_cstr(res, "(./)");
                  rc_is_valid = 0;
                  break;
              case rm_count_valid:
                  res = secstrcat_string(res, startp->rm_msg);
                  res = secstrcat_cstr(res, "\n");
                  break;
              case rm_count_invalid:
                  res = secstrcat_string(res, startp->rm_msg);
                  res = secstrcat_cstr(res, "\n");
                  break;
              case rm_summary_valid:
                  res = secstrcat_cstr(res, "\n(./) ");
                  res = secstrcat_string(res, startp->rm_msg);
                  break;
              case rm_summary_invalid:
                  res = secstrcat_cstr(res, "\n(EE)");
                  res = secstrcat_string(res, startp->rm_msg);
                  break;
              default:
                  res = secstrcat_string(res, startp->rm_msg);
          }
      }
      startp = startp->next;
  }
  res=secstrcat_cstr( res, "\n");
  return res;
}
/* vim: set tabstop=2 softtabstop=2 shiftwidth=2 smarttab expandtab :*/
