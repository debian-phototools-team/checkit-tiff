/* 'checkit_tiff' is a conformance checker for baseline TIFFs
 *
 * author: Andreas Romeyke, 2015-2022
 * licensed under conditions of libtiff
 * (see http://libtiff.maptools.org/misc.html)
 *
 */

#include "check.h"
#include "check_helper.h"
#include "ctstring.h"


/* check if IFDs are word aligned */
ret_t check_all_IFDs_are_word_aligned(const ctiff_t * ctif) {
  GET_EMPTY_RET(ret);
  TIFP_CHECK( ctif, ret);

  uint32 ifd = get_ifd0_pos( ctif ); /*  TODO: check all other IFDs, too */
    if (0 == (ifd & 1)) {
        ret.returncode = is_valid;
        return ret;
    } else {
        // FIXME: tif_fails?
        ret = set_value_found_ret_formatted(&ret, "offset of first IFD points to 0x%08x and is not word-aligned", ifd);
        ret.returncode = ifderror_offset_not_word_aligned;
        return ret;
    }
}
/* vim: set tabstop=2 softtabstop=2 shiftwidth=2 smarttab expandtab :*/
