Support to check colorprofile
=============================

== General information

the ICC validation code is very alpha. Please also check the ICC Signature
Registry at http://www.color.org/signatures2.xalter[] and inform me if you find a
bug in 'checkit_tiff'.

== IccProfLib

The 'checkit_tiff' comes with support for ICC reference implementation library IccProfLib2. It the library is not detected by cmake, checkit_tiff uses a rudimentary implementation as fallback.

The IccProfLib is available at https://github.com/InternationalColorConsortium/DemoIccMAX/tree/master/IccProfLib[].

To disable to use a proflib, set "WITHOUT_ICC_PROFLIB".

== Strong vs relaxed validation

The ICC favors relaxed validation because the basic behavior only changes between major versions of the ICC profiles. This is now also the default in checkit_tiff.
In this mode only the major version is checked and warnings of the ICCProflib2 are not evaluated as errors.

the corresponding compile flag must be set: 

* "WITH_STRONG_ICC" -  only the current ICC color profiles are allowed and warnings of the ICCProflib2 are evaluated as errors.

If one wants to check test settings of files, it makes sense to check these strictly, in order to affect producers from the outset.
This mode is strongly recommended if you use checkit_tiff to validate a test set from a digitization service provider.

The flag is not compatible with "ALLOW_ICC1_2001_04".

== Outdated color profiles

To support outdated ICC versions, the corresponding compile flags must be set:

* "ALLOW_CMMTYPE_LINO" - this allows "lino" as preferred CMM type
* "ALLOW_ICC1_2001_04" - this allows outdated v2.4.0 profiles

See link:README.compile[] for details.



